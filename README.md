# xon-radio-next-song

Creates HTTP server that returns random line from a file

## Installation

```bash
yarn global add xon-radio-next-song
# or
npm install -g xon-radio-next-song
```

## Usage

### Options

```
--file     path to a file to read lines from                        [required]
--port     port to listen on                                   [default: 8080]
--host     host to listen on                            [default: "127.0.0.1"]
--help     Show help                                                 [boolean]
--version  Show version number                                       [boolean]
```

### Example

1. Start HTTP server:
  ```bash
  $ xon-radio-next-song --file=$HOME/songs.txt
  File read from /home/user/songs.txt (516 lines)
  Listening on http://127.0.0.1:8080/
  ```

2. Get next song:
  ```bash
  $ curl 'http://127.0.0.1:8080/'
  http://jeffs.eu/radio/radio-royalty-free-c4734876-56c3-11e8-b837-94de80b1da5c.pk3   c4734876-56c3-11e8-b837-94de80b1da5c.ogg 287.19 ccMixter - Not So Happy Holidays   (Stefan Kartenberg Mix) - http://www.jamendo.com
  ```

## Enable radio in Xonotic

1. You need to be running an [Jeff's mods](https://gitlab.com/sc-breakfast/jeff-modpack) or [SMB Modpack](https://github.com/MarioSMB/modpack).

2. Add this to your server config:
  ```
  // Radio
  set sv_radio 1
  set sv_radio_queue_autofill 1
  set sv_radio_queue_autofill_server "http://localhost:8080"
  ```

## Use as systemd service

1. Create new file `/etc/systemd/system/xonotic-radio.sertice`:
  ```
  [Unit]
  Description=Xonotic Radio Service
  After=network.target

  [Service]
  Type=simple
  Restart=always
  User=user
  WorkingDirectory=/home/user
  ExecStart=/usr/bin/xon-radio-next-song --file=/home/user/songs.txt

  [Install]
  WantedBy=multi-user.target
  ```

2. Reload services:
  ```bash
  $ systemctl daemon-reload
  ```

3. Enable service:
  ```bash
  $ systemd enable xonotic-radio.sertice
  ```

4. Start service:
  ```bash
  $ systemd start xonotic-radio.sertice
  ```
